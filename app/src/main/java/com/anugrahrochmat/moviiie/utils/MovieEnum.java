package com.anugrahrochmat.moviiie.utils;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version MovieEnum, v 0.1 2019-11-28 16:56 by Tyas Anugrah Rochmat
 */
public enum MovieEnum {

    POPULAR("popular"), TOP_RATED("top_rated");

    private final String sortBy;

    MovieEnum(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortBy() {
        return this.sortBy;
    }

}
