package com.anugrahrochmat.moviiie.data.network;

import com.anugrahrochmat.moviiie.BuildConfig;
import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version Service, v 0.1 2019-11-28 14:43 by Tyas Anugrah Rochmat
 */
public class Service {
    private static final String APIKEY = BuildConfig.APIKEY;
    private final NetworkInterface networkInterface;

    Service(NetworkInterface networkInterface) {
        this.networkInterface = networkInterface;
    }

    public Subscription getMovieList(String sortBy, final GetMovieListCallback callback) {

        return networkInterface.getMovieList(sortBy, APIKEY)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext(new Func1<Throwable, Observable<? extends MovieListResponse>>() {
                @Override
                public Observable<? extends MovieListResponse> call(Throwable throwable) {
                    return Observable.error(throwable);
                }
            })
            .subscribe(new Subscriber<MovieListResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    callback.onError(new NetworkError(e));

                }

                @Override
                public void onNext(MovieListResponse movieListResponse) {
                    callback.onSuccess(movieListResponse);

                }
            });
    }

    public interface GetMovieListCallback{
        void onSuccess(MovieListResponse movieListResponse);

        void onError(NetworkError networkError);
    }
}
