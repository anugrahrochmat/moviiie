package com.anugrahrochmat.moviiie.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version MovieListResponse, v 0.1 2019-11-28 11:19 by Tyas Anugrah Rochmat
 */
public class MovieListResponse {
    @SerializedName("results")
    private List<Movie> results = null;
    @SerializedName("page")
    private Integer page;
    @SerializedName("total_results")
    private Integer totalResults;
    @SerializedName("total_pages")
    private Integer totalPages;

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
