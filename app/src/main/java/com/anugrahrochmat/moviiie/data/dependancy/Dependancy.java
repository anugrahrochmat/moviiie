package com.anugrahrochmat.moviiie.data.dependancy;

import com.anugrahrochmat.moviiie.data.network.NetworkModule;
import com.anugrahrochmat.moviiie.presentation.main.MainActivity;
import com.anugrahrochmat.moviiie.presentation.popular.PopularActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version Dependancy, v 0.1 2019-11-28 15:13 by Tyas Anugrah Rochmat
 */
@Singleton
@Component(modules = {NetworkModule.class,})
public interface Dependancy {
    void inject(MainActivity mainActivity);
    void inject(PopularActivity popularActivity);
}
