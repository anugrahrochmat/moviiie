package com.anugrahrochmat.moviiie.data.entity;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version Movie, v 0.1 2019-11-28 10:40 by Tyas Anugrah Rochmat
 */
public class Movie implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @SerializedName("popularity") private Double popularity;
    @SerializedName("vote_count") private Integer voteCount;
    @SerializedName("poster_path") private String posterPath;
    @SerializedName("id") private Integer id;
    @SerializedName("backdrop_path") private String backdropPath;
    @SerializedName("original_language") private String originalLanguage;
    @SerializedName("original_title") private String originalTitle;
    @SerializedName("title") private String title;
    @SerializedName("vote_average") private Double voteAverage;
    @SerializedName("overview") private String overview;
    @SerializedName("release_date") private String releaseDate;

    /**
     * Constructor
     */
    public Movie(Double popularity, Integer voteCount, String posterPath, Integer id,
                 String backdropPath, String originalLanguage, String originalTitle, String title,
                 Double voteAverage, String overview, String releaseDate) {
        this.popularity = popularity;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.id = id;
        this.backdropPath = backdropPath;
        this.originalLanguage = originalLanguage;
        this.originalTitle = originalTitle;
        this.title = title;
        this.voteAverage = voteAverage;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }

    /**
     * Getter and Setter
     */
    public Double getPopularity() {
        return popularity;
    }
    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Integer getVoteCount() {
        return voteCount;
    }
    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }


    /**
     * Parcelling
     */
    public Movie(Parcel in){
        this.popularity = in.readDouble();
        this.voteCount = in.readInt();
        this.posterPath = in.readString();
        this.id = in.readInt();
        this.backdropPath = in.readString();
        this.originalLanguage = in.readString();
        this.originalTitle = in.readString();
        this.title = in.readString();
        this.voteAverage = in.readDouble();
        this.overview = in.readString();
        this.releaseDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(popularity);
        dest.writeValue(voteCount);
        dest.writeValue(posterPath);
        dest.writeValue(id);
        dest.writeValue(backdropPath);
        dest.writeValue(originalLanguage);
        dest.writeValue(originalTitle);
        dest.writeValue(title);
        dest.writeValue(voteAverage);
        dest.writeValue(overview);
        dest.writeValue(releaseDate);
    }

    @Override
    public String toString() {
        return "Movie{" +
            "popularity='" + popularity + '\'' +
            ", voteCount='" + voteCount + '\'' +
            ", posterPath='" + posterPath + '\'' +
            ", id='" + id + '\'' +
            ", backdropPath='" + backdropPath + '\'' +
            ", originalLanguage='" + originalLanguage + '\'' +
            ", originalTitle='" + originalTitle + '\'' +
            ", title='" + title + '\'' +
            ", voteAverage='" + voteAverage + '\'' +
            ", overview='" + overview + '\'' +
            ", releaseDate='" + releaseDate + '\'' +
            '}';
    }
}