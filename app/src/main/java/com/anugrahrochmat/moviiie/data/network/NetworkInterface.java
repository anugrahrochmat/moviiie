package com.anugrahrochmat.moviiie.data.network;

import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version NetworkInterface, v 0.1 2019-11-28 10:37 by Tyas Anugrah Rochmat
 */
public interface NetworkInterface {

    @GET("{sort_by}")
    Observable<MovieListResponse> getMovieList(@Path("sort_by") String sortBy, @Query("api_key") String apiKey);

}
