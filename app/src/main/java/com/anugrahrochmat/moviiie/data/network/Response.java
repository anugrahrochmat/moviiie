package com.anugrahrochmat.moviiie.data.network;

import com.google.gson.annotations.SerializedName;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version Response, v 0.1 2019-11-28 16:04 by Tyas Anugrah Rochmat
 */
public class Response {

    @SerializedName("status")
    public String status;

    @SuppressWarnings({"unused", "used by Retrofit"})
    public Response() {
    }

    public Response(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
