package com.anugrahrochmat.moviiie.presentation.main;

import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version MainView, v 0.1 2019-11-28 11:52 by Tyas Anugrah Rochmat
 */
public interface MainView {

    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getMovieListSuccess(MovieListResponse movieListResponse);
}
