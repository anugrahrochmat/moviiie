package com.anugrahrochmat.moviiie.presentation.splashscreen;

import com.anugrahrochmat.moviiie.R;
import com.anugrahrochmat.moviiie.presentation.popular.PopularActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        long delayTime = 4000;
        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, PopularActivity.class);
                startActivity(intent);
            }
        }, delayTime);

    }
}
