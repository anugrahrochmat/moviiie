package com.anugrahrochmat.moviiie.presentation.popular;

import com.anugrahrochmat.moviiie.BuildConfig;
import com.anugrahrochmat.moviiie.R;
import com.anugrahrochmat.moviiie.data.entity.Movie;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version PopularAdapter, v 0.1 2019-11-28 17:41 by Tyas Anugrah Rochmat
 */
public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.ViewHolder> {

    private static final String BASEPOSTERURL = BuildConfig.BASEPOSTERURL;

    private List<Movie> movieList;
    private Context context;

    PopularAdapter(Context context, List<Movie> movieList) {
        this.movieList = movieList;
        this.context = context;
    }

    @NotNull
    @Override
    public PopularAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PopularAdapter.ViewHolder holder, int position) {
        String strImg = movieList.get(position).getPosterPath();
        Picasso.get().load(BASEPOSTERURL+strImg).into(holder.imgMovie);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
        notifyDataSetChanged();
    }

    List<Movie> getMovieList() {
        return movieList;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_movie)   ImageView imgMovie;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Movie movie = movieList.get(getAdapterPosition());
            Toast.makeText(context, movie.getOriginalTitle(), Toast.LENGTH_SHORT).show();
        }
    }

}
