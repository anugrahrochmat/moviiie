package com.anugrahrochmat.moviiie.presentation.popular;

import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version PopularView, v 0.1 2019-11-28 17:26 by Tyas Anugrah Rochmat
 */
public interface PopularView {

    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getMovieListSuccess(MovieListResponse movieListResponse);
}
