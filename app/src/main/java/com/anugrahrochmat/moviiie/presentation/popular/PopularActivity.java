package com.anugrahrochmat.moviiie.presentation.popular;

import com.anugrahrochmat.moviiie.BuildConfig;
import com.anugrahrochmat.moviiie.R;
import com.anugrahrochmat.moviiie.data.entity.Movie;
import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;
import com.anugrahrochmat.moviiie.data.network.Service;
import com.anugrahrochmat.moviiie.presentation.base.BaseApp;
import com.anugrahrochmat.moviiie.presentation.main.MainPresenter;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PopularActivity extends BaseApp implements PopularView {

    @Inject
    public Service service;

    @BindView(R.id.rv_movie_list)   RecyclerView rvMovieList;
    @BindView(R.id.progress)        ProgressBar progressBar;
//    @BindView(R.id.tv_test)         TextView tvTest;

    private final String SAVED_POPULAR_MOVIES = "SAVED_POPULAR_MOVIES";
    private PopularPresenter presenter;
    private PopularAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular);
        ButterKnife.bind(this);

        getDependancy().inject(this);

        init();



    }

    private void init(){
        rvMovieList.setLayoutManager(new GridLayoutManager(this, 2));

        adapter = new PopularAdapter(getApplicationContext(), new ArrayList<Movie>());

        presenter = new PopularPresenter(service, this);
        presenter.getMovieList();
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void getMovieListSuccess(MovieListResponse movieListResponse) {
        adapter = new PopularAdapter(getApplicationContext(), movieListResponse.getResults());
        rvMovieList.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Movie> moviesSaved = new ArrayList<>(adapter.getMovieList());
        if (!moviesSaved.isEmpty()) {
            outState.putParcelableArrayList(SAVED_POPULAR_MOVIES, moviesSaved);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SAVED_POPULAR_MOVIES)){
                List<Movie> movies = savedInstanceState.getParcelableArrayList(SAVED_POPULAR_MOVIES);
                adapter.setMovieList(movies);
            }
        } else {
            presenter.getMovieList();
        }
    }
}
