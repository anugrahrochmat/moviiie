package com.anugrahrochmat.moviiie.presentation.main;

import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;
import com.anugrahrochmat.moviiie.data.network.NetworkError;
import com.anugrahrochmat.moviiie.data.network.Service;
import com.anugrahrochmat.moviiie.utils.MovieEnum;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version MainPresenter, v 0.1 2019-11-28 11:52 by Tyas Anugrah Rochmat
 */
public class MainPresenter {

    private final Service service;
    private final MainView view;
    private CompositeSubscription subscriptions;


    public MainPresenter(Service service, MainView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }

    public void getMovieList() {
        view.showWait();

        Subscription subscription = service.getMovieList(MovieEnum.POPULAR.getSortBy(), new Service.GetMovieListCallback() {
            @Override
            public void onSuccess(MovieListResponse movieListResponse) {
                view.removeWait();
                view.getMovieListSuccess(movieListResponse);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
                view.onFailure(networkError.getAppErrorMessage());
            }

        });

        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
