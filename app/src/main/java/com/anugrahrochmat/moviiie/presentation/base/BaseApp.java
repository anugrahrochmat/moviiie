package com.anugrahrochmat.moviiie.presentation.base;

import com.anugrahrochmat.moviiie.data.dependancy.DaggerDependancy;
import com.anugrahrochmat.moviiie.data.dependancy.Dependancy;
import com.anugrahrochmat.moviiie.data.network.NetworkModule;

import android.annotation.SuppressLint;
import android.os.Bundle;

import java.io.File;

import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version BaseApp, v 0.1 2019-11-28 15:14 by Tyas Anugrah Rochmat
 */
@SuppressLint("Registered")
public class BaseApp extends AppCompatActivity {
    Dependancy dependancy;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        dependancy = DaggerDependancy.builder().networkModule(new NetworkModule(cacheFile)).build();

    }

    public Dependancy getDependancy() {
        return dependancy;
    }
}
