package com.anugrahrochmat.moviiie.presentation.main;


import com.anugrahrochmat.moviiie.R;
import com.anugrahrochmat.moviiie.data.entity.Movie;
import com.anugrahrochmat.moviiie.data.entity.MovieListResponse;
import com.anugrahrochmat.moviiie.data.network.Service;
import com.anugrahrochmat.moviiie.presentation.base.BaseApp;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.inject.Inject;


public class MainActivity extends BaseApp implements MainView{

    @Inject
    public Service service;
    private ProgressBar progressBar;
    private ImageView imageView;
//    private TextView tvTest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        tvTest = findViewById(R.id.tv_test);
        imageView = findViewById(R.id.img_test);
        progressBar = findViewById(R.id.progress);

        getDependancy().inject(this);


        MainPresenter presenter = new MainPresenter(service, this);
        presenter.getMovieList();
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String appErrorMessage) {

    }

    @Override
    public void getMovieListSuccess(MovieListResponse movieListResponse) {
//        Movie movie = movieListResponse.getResults().get(1);
//        tvTest.setText(movie.getOriginalTitle());
        Picasso.get().setLoggingEnabled(true);
        Picasso.get().load("http://image.tmdb.org/t/p/w342//2bXbqYdUdNVa8VIWXVfclP2ICtT.jpg").into(imageView);
//        Picasso.get().load(R.drawable.spdman).into(imageView);
    }
}
