package com.anugrahrochmat.moviiie.presentation;

import com.anugrahrochmat.moviiie.BuildConfig;
import com.squareup.leakcanary.LeakCanary;

import android.app.Application;

import timber.log.Timber;

/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version AndroidApplication, v 0.1 2019-11-28 10:00 by Tyas Anugrah Rochmat
 */
public class AndroidApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
//        initializeLeakCanary();
        initializeTimber();
    }

    private void initializeTimber() {
        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initializeLeakCanary() {
        if (BuildConfig.DEBUG) {
            LeakCanary.install(this);
        }
    }
}
